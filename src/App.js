import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";

import {BrowserRouter, Routes, Route } from "react-router-dom";


import Index from './pages/index.js';
import Cars from './pages/cars.js'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element ={<Index />}/>
        <Route path="/cars" element ={<Cars />}/>
      </Routes>
    </BrowserRouter>

  );
}

export default App;
